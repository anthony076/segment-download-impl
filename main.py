# target https://vjs.zencdn.net/v/oceans.mp4

import asyncio, os
from os.path import isfile
from aiohttp import ClientSession
from math import floor
from glob import glob
from typing import List

# =============================
# 工具函數 
# =============================
def checkComplete(resultList:List[bool]) :
    """
    檢查是否所有分段檔案都下載完畢
    """
    for item in resultList:
        if item == False:
            return False
    
    return True

def genSegment(totalSize:int, split:int=3) -> List[int]:
    """
    if file-size = 12 bytes, split = 3 , then segments will be [0, 4, 8, 12]
    計算各分段的索引值
    """

    step = floor(totalSize / split)
    assert step >= 0, "step cant smaller than 1"

    segments:List[int] = [ n for n in range(0, totalSize, step) ]
    segments.append(totalSize)  # insert end-point

    return segments

def deleteAll(fileList:List[str]):
    """
    成功合併檔案後，刪除所有的分段檔
    """
    for path in fileList:
        os.remove(path)

def combineAll(filename:str):
    """
    結合所有的分段檔
    """
    raw = b""

    fileList = glob("*.tmp")
    assert len(fileList) > 0, "file not found"

    fileList.sort(key=lambda item:int(item.split(".")[0]))

    for bfile in fileList:
        print(bfile)

        with open(bfile, "rb") as fd:
            raw += fd.read()

    with open(filename, "wb") as fw:
        fw.write(raw)

    if isfile(filename):
        deleteAll(fileList)
        print(f"combine {filename} ok ...")

# =============================
# 流程函數 
# =============================
async def check(url:str, split:int=3) -> dict:
    """ 
    get allow_partial、content_size、total_size、filename、segments
    判斷是否支援分段下載、並取得檔案名、檔案大小和各分段的索引
    """

    allow_partial:bool = False
    headers:dict = {'Range': 'bytes=0-9'}

    async with ClientSession() as session:
        async with session.get(url, headers=headers) as resp:
            
            # 透過 resp-headers 取得 binary-content 的長度
            content_size:int = int(resp.headers["Content-Length"])
            
            # 透過 resp-headers 取得 檔案的實際長度
            total_size:int = int(resp.headers["Content-Range"].split("/")[1])

            # 讀取回應的狀態碼
            if resp.status == 206 :
                allow_partial = True
            
    return { 
        "filename": url.split("/")[-1],
        "url": url, 
        "allow_partial": allow_partial, 
        "content_size":content_size,
        "total_size": total_size, 
        "segments" : genSegment(total_size, split),        
    }

async def _get(url:str, start:int, end:int):
    """
    發出 req 取回分段檔案
    """
    print(f"[seg] {start}-{end} segment start ...")

    headers:dict = {'Range': f'bytes={start}-{end}'}

    try:
        async with ClientSession() as session:
            async with session.get(url, headers=headers) as resp:
                # 讀取回應的 binary-content
                raw = await resp.read()

        # write segment into file
        with open(f"{start}.tmp", "wb") as fw:
            fw.write(raw)

        return True

    except:
        return False

async def download(info:dict):
    """
    開放下載的接口
    """
    tasks = []
    segments = info["segments"]
    filename = info["filename"]

    # ==== download ====
    for i in range(len(segments)-1):
        start = segments[i]
        end = segments[i+1] - 1

        task = asyncio.create_task(_get(info["url"], start=start, end=end))
        tasks.append(task)

    resultList = await asyncio.gather(*tasks, return_exceptions=True)
    isComplete = checkComplete(resultList)

    # ==== write to file if all segment is done ====
    if isComplete:
        combineAll(filename)
        print( f"{filename} is done")
    else:
        print( f"{filename} is fail")

async def main():
    url = "https://vjs.zencdn.net/v/oceans.mp4"
    
    # ==== step1，check before download ====
    info = await check(url, split=10)
    print(info)

    # ==== step2，execute download ====
    result = await download(info)
    
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
